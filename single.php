<?php get_header(); ?>

	<div>
    
    	<?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
        
        <div <?php post_class(); ?>>
        	<div class="content">
            	<h2 class="title"><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </div>
        </div>
        
        <?php endwhile; ?>
        <?php endif; ?>
    
    </div>

<?php get_footer(); ?>